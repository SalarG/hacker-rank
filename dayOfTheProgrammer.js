'use strict';

const fs = require('fs');

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', function(inputStdin) {
    inputString += inputStdin;
});

process.stdin.on('end', function() {
    inputString = inputString.split('\n');

    main();
});

function readLine() {
    return inputString[currentLine++];
}

// Complete the dayOfProgrammer function below.
function dayOfProgrammer(y) {
    let date;

 if (((y <= 1917) && (y % 4 === 0)) ||( (y % 4 === 0) && (y % 100 !== 0) )|| (y % 400 === 0)) {
        date =`12.09.${y}`
    } else if (y === 1918) {
        date =`26.09.1918`
    }else {
        date=`13.09.${y}`
    }
    return date;
    console.log(date);
}

function main() {
    const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

    const year = parseInt(readLine().trim(), 10);

    const result = dayOfProgrammer(year);

    ws.write(result + '\n');

    ws.end();
}
