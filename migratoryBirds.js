'use strict';

const fs = require('fs');

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', function(inputStdin) {
    inputString += inputStdin;
});

process.stdin.on('end', function() {
    inputString = inputString.split('\n');

    main();
});

function readLine() {
    return inputString[currentLine++];
}

// Complete the migratoryBirds function below.
function migratoryBirds(birds) {
    // const lines = input.split('\n');
    // const birds = lines[1].split(' ').map(i => parseInt(i));
    const birdsCounts = [];
    for (let i = 0; i < birds.length; i++) {
        if (birdsCounts[birds[i]]) {
            birdsCounts[birds[i]] ++;
        } else {
            birdsCounts[birds[i]] = 1
        }
    }

    let maxBirdType = 0;
    let maxBirdTypeCount = 0;
    for (let i = 0; i < birds.length; i++) {
        if (birdsCounts[birds[i]] >= maxBirdTypeCount) {
            maxBirdTypeCount = birdsCounts[birds[i]];
            maxBirdType = birds[i]
        }
    }

    return maxBirdType;
};


function main() {
    const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

    const arrCount = parseInt(readLine().trim(), 10);

    const arr = readLine().replace(/\s+$/g, '').split(' ').map(arrTemp => parseInt(arrTemp, 10));

    const result = migratoryBirds(arr);

    ws.write(result + '\n');

    ws.end();
}
